import json

# Імітація початкових даних
initial_data = {
    "people": [
        {"surname": "Doe", "phone": "1234567890"},
        {"surname": "Smith", "phone": "2345678901"},
        # Додайте інші записи, які представляють людей
    ]
}


# Збереження початкових даних у JSON файл
def save_to_json(data, filename):
    with open(filename, 'w') as json_file:
        json.dump(data, json_file, indent=2)
    print(f"Data saved to {filename}")


# Зчитування даних з JSON файлу
def load_from_json(filename):
    try:
        with open(filename, 'r') as json_file:
            data = json.load(json_file)
        return data
    except FileNotFoundError:
        print(f"Error: File '{filename}' not found.")
        return None
    except json.JSONDecodeError as json_err:
        print(f"JSON Decode Error: {json_err}")
        return None


# Додавання нового запису до JSON файлу
def add_record(data, surname, phone):
    new_record = {"surname": surname, "phone": phone}
    data["people"].append(new_record)
    print("Record added successfully.")


# Видалення запису за прізвищем з JSON файлу
def delete_record(data, surname):
    for person in data["people"]:
        if person["surname"] == surname:
            data["people"].remove(person)
            print(f"Record for {surname} deleted successfully.")
            return
    print(f"No record found for {surname}.")


# Пошук за телефонним номером
def search_by_phone(data, phone):
    for person in data["people"]:
        if person["phone"] == phone:
            print(f"Phone {phone} belongs to {person['surname']}.")
            return
    print(f"No record found for phone {phone}.")


# Пошук за прізвищем
def search_by_surname(data, surname):
    for person in data["people"]:
        if person["surname"] == surname:
            print(f"{surname}'s phone number is {person['phone']}.")
            return
    print(f"No record found for {surname}.")


# Виконання задачі відповідно до варіанту
def solve_task(data, phone):
    for person in data["people"]:
        if person["phone"] == phone:
            return {"surname": person["surname"], "phone": person["phone"]}
    return None


# Головна функція для виклику в діалоговому режимі
def main():
    json_filename = "phone_book.json"

    # Завантаження або створення початкових даних
    data = load_from_json(json_filename)
    if data is None:
        data = initial_data
        save_to_json(data, json_filename)

    while True:
        print("\nOptions:")
        print("1. Display JSON content")
        print("2. Add new record")
        print("3. Delete record by surname")
        print("4. Search by phone number")
        print("5. Search by surname")
        print("6. Solve task and save result to another JSON file")
        print("7. Exit")

        choice = input("Enter your choice (1-7): ")

        if choice == "1":
            print(json.dumps(data, indent=2))
        elif choice == "2":
            surname = input("Enter surname: ")
            phone = input("Enter phone number: ")
            add_record(data, surname, phone)
        elif choice == "3":
            surname = input("Enter surname to delete: ")
            delete_record(data, surname)
        elif choice == "4":
            phone = input("Enter phone number to search: ")
            search_by_phone(data, phone)
        elif choice == "5":
            surname = input("Enter surname to search: ")
            search_by_surname(data, surname)
        elif choice == "6":
            phone = input("Enter phone number for the task: ")
            result = solve_task(data, phone)
            if result:
                save_to_json({"result": result}, "task_result.json")
                print("Task result saved to task_result.json")
            else:
                print(f"No record found for phone {phone}.")
        elif choice == "7":
            save_to_json(data, json_filename)
            print("Exiting the program. Data saved.")
            break
        else:
            print("Invalid choice. Please enter a number between 1 and 7.")


if __name__ == "__main__":
    main()
